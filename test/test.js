const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../server");
const bodyParser = require('body-parser');
const fs = require('fs');
const express = require('express');

var assert = chai.assert;
var expect = chai.expect;
chai.use(chaiHttp);


//ucitava podatke iz datoteke testniPodaci.txt
 let podaci = fs.readFileSync('./testniPodaci.txt', 'utf-8');
 let testniPodaci = podaci.split("\n");


for(var i = 0; i<testniPodaci.length; i++){
    let currentLine = testniPodaci[i].split(",");
    console.log(currentLine);
if(!testniPodaci[i]) continue;
if(currentLine[0] == 'DELETE') {
   if(currentLine[1] == "/all"){
       describe('brisanje', () => {
           describe('DELETE /all', () => {
             it('testira brisanje sadrzaja datoteka predmeti.txt i aktivnosti.txt', ()=>{
                 chai.request('http://localhost:3000')
                 .delete(currentLine[1])
                 .end((err, res) => {
                    assert.equal(JSON.stringify(res.body) + "\r" ,  currentLine[3].replace(/\\/g, ""));
                });
             })
           });
       });
   }
   else {
    let pomocna = currentLine[1].split("/");
    let zaSlanje = "/" + pomocna[1] + "/" + currentLine[2] ;
    console.log(zaSlanje);
    describe('brisanje', () => {
        describe('DELETE /nesto/:naziv', () => {
          it('testira brisanje predemeta ili aktivnosti', ()=>{
              chai.request('http://localhost:3000')
              .delete(zaSlanje)
              .end((err, res) => {
                 assert.equal(JSON.stringify(res.body) + "\r" ,  currentLine[3].replace(/\\/g, ""));
             });
          })
        });
    });
   }
}
else if(currentLine[0] == 'GET'){
  describe('geteri', () => {
        if(currentLine[1] == "/predmeti" || currentLine[1]=="/aktivnosti"){
            let spajanje = currentLine[3].replace(/\\/g, "");
            if(currentLine.length>4){
              for(let i = 4; i<currentLine.length; i++) {
                 spajanje+="," + currentLine[i].replace(/\\/g, "");
              }
            }
        describe('GET /nesto', () =>{
            it('testira get za predmete i aktivnosti pojedinacno', function(){
                chai.request('http://localhost:3000')
                .get(currentLine[1])
                .end((err, res) => {
                    assert.equal(JSON.stringify(res.body) + "\r", spajanje);
                });
            })
        });
    }
    else if(currentLine[1] == "/predmet/:naziv/aktivnost/"){
         let pomocna = currentLine[1].split("/");
         let zaSlanje = "/" + pomocna[1] + "/" + currentLine[2] + "/" + pomocna[3] + "/";
         let spajanje = currentLine[3].replace(/\\/g, "");
         if(currentLine.length>4) {
            for(let i = 4; i<currentLine.length; i++) {
                spajanje+="," + currentLine[i].replace(/\\/g, "");
             }
         }
         describe('GET /aktivnosti za predmet', () =>{
            it('testira get za niz aktivnosti za zadani predmet', function(){
                chai.request('http://localhost:3000')
                .get(zaSlanje)
                .end((err, res) => {
                    assert.equal(JSON.stringify(res.body) + "\r", spajanje);
                });
            })
        });
    }
});
}
else if(currentLine[0] == "POST") {
    describe('dodavanje predmeta i aktivnosti', () => {
        if(currentLine.length == 4){  
        let zaSlanje = currentLine[2].toString();
        let zaSlanje1 = zaSlanje.replace(/\\/g, "");
        describe('POST /nesto', () =>{
            it('testira dodavanje aktivnosti i predmeta', function(){
              chai.request('http://localhost:3000')
              .post(currentLine[1])
              .send(JSON.parse(zaSlanje1))
              .end((err, res) => {
                assert.equal(JSON.stringify(res.body) + "\r", currentLine[3].replace(/\\/g, ""));
            });
            })
        });
    }
    else {
        let pomocna = currentLine[2];
        for(let j = 3; j<currentLine.length-1; j++) {
            pomocna +="," + currentLine[j];
        }
        let nova = pomocna.toString().replace(/\\/g, "");
        describe('POST /nesto', () =>{
            it('testira dodavanje aktivnosti i predmeta', function(){
              chai.request('http://localhost:3000')
              .post(currentLine[1])
              .send(JSON.parse(nova))
              .end((err, res) => {
                assert.equal(JSON.stringify(res.body) + "\r", currentLine[currentLine.length-1].replace(/\\/g, ""));
            });
            })
        });
    }
    });
}
}




