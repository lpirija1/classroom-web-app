
    const express = require('express');
    const app = express();
    const bodyParser = require('body-parser');
    const fs = require('fs');
    const db = require('./baza');

    app.use(express.static('public'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/v1/predmeti', (req, res) => {
        fs.readFile('predmeti.txt', (err, data) => {
            if (err) throw err;
            //JSON.parse()
            var podaci = data.toString('utf8');
            const lines = podaci.split('\n');
            const result = [];
            const headers = 'naziv';
            for (let i = 1; i < lines.length; i++) {        
                if (!lines[i])
                    continue;
                const obj = {};
                    obj[headers] = lines[i];
                result.push(obj);
            }
            
        
        //return result; //JavaScript object
        var json =  JSON.stringify(result); //JSON
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(json);
            res.end();
        });
        
    });
    app.get('/v1/aktivnosti', (req, res) => {
        fs.readFile('aktivnosti.txt', (err, data) => {
            if (err) throw err;
            //JSON.parse()
            var podaci = data.toString('utf8');
            const lines = podaci.split('\n');
            const result = [];
            const headers = ['naziv','tip','pocetak','kraj','dan'];
            
            for (let i = 1; i < lines.length; i++) {        
                if (!lines[i])
                    continue;
                const obj = {};
                const currentline = lines[i].split(',');
        
                for (let j = 0; j < headers.length; j++) {
                    obj[headers[j]] = currentline[j];
                }
                result.push(obj);
            }
            
        
        //return result; //JavaScript object
        var json =  JSON.stringify(result); //JSON
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(json);
            
        });
    });

    app.get('/v1/predmet/:naziv/aktivnost/', (req, res) => {
        var predmet = req.params.naziv;
        fs.readFile('aktivnosti.txt', (err, data) => {
            if (err) throw err;
            //JSON.parse()
            var podaci = data.toString('utf8');
            const lines = podaci.split('\n');
            const result = [];
            const headers = ['naziv','tip','pocetak','kraj','dan'];
            
            for (let i = 1; i < lines.length; i++) {        
                if (!lines[i])
                    continue;
                const obj = {};
                const currentline = lines[i].split(',');
        
                for (let j = 0; j < headers.length; j++) {
                    if(currentline[0].localeCompare(predmet.toString())==0){
                    obj[headers[j]] = currentline[j];
                    }
                    else{
                        obj[headers[j]]="";
                    }
                }
                if(obj['naziv']!=""){
                result.push(obj);
                }
            }
            
        
        //return result; //JavaScript object
        var json =  JSON.stringify(result); //JSON
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(json);
            
        });
    });


    app.post('/v1/predmet', (req, res) => {
        var a = req.body;
        var linija = "\n" + a['naziv'];
        var naziv = a['naziv'];
        var logicka = 0;
        fs.readFile("predmeti.txt", function (err, data) {
            if (err) throw err;
            var podaci = data.toString('utf8');
            const lines = podaci.split('\n')
        
        for (let i = 1; i < lines.length; i++) {        
            if (!lines[i])
                continue;
            else if(lines[i].localeCompare(naziv)==0){
                logicka = 1;
                return res.json({message:"Naziv predmeta postoji!"});
                
            }
        }
        if(logicka==0){
            fs.appendFile('predmeti.txt', linija, (error) => { 
                // In case of a error throw err exception.
                if (error) throw err; 
                return res.json({message:"Uspješno dodan predmet!"});
               
            });
            }
            });   
        });    



    app.post('/v1/aktivnost', (req, res) => {
        var a = req.body;
        var linija ="\n" +  a['naziv'] + ","+ a['tip'] + "," + a['pocetak']+ "," + a['kraj'] + "," + a['dan'];
        var naziv = a['naziv'];
        var dan = a['dan'];
        var tip = a['tip'];

        //provjeriti necijele sate i pocetak rasporeda
        if(naziv==null || naziv.localeCompare("")==0 || tip==null || tip.localeCompare("")==0 || dan==null || dan.localeCompare("")==0 || a.kraj<a.pocetak || a.pocetak<0 || a.pocetak>24 || a.kraj<0 || a.kraj>24){
            return res.json({message: "Aktivnost nije validna!"});   
            
        }
        else{
        //preklapanje
        fs.readFile('aktivnosti.txt', (err, data) => {
            if (err) throw err;
            var podaci = data.toString('utf8');
            const lines = podaci.split('\n');
            var i  = 1;
            for (i = 1; i < lines.length; i++) {        
                if (!lines[i])
                    continue;
                const currentline = lines[i].split(',');
                    if(currentline[4].localeCompare(dan)==0){
                        if(currentline[2]<=a.pocetak && currentline[3]>=a.kraj || currentline[2]>=a.pocetak && currentline[2]<=a.kraj || currentline[2]<=a.pocetak && a.kraj>=currentline[3]){
                        return res.json({message: "Aktivnost nije validna!"}); 
                        
                        }
                    }
                }
                if(i==lines.length){
                    fs.appendFile('aktivnosti.txt', linija, (error) => { 
                        // In case of a error throw err exception.
                        if (error) throw err; 
                        return res.json({message:"Uspješno dodana aktivnost!"});     
                });
                }
            });
    }
    });


        app.delete('/v1/aktivnost/:naziv', (req, res) => { 
            let brojac = 0;
            fs.readFile('aktivnosti.txt', (err, data) => {
                if (err) return err; 
                let podaci = data.toString('utf8');
                const lines = podaci.split('\n');
                        fs.writeFile('aktivnosti.txt', '', function(){
                            if(err) return err; 
                        });

                for(let i = 0; i<lines.length; i++) {
                    let currentline = lines[i].split(",");
                        if(currentline[0].localeCompare(req.params.naziv.toString()) != 0){
                            let dodaj = lines[i] + "\n";
                            fs.appendFile('aktivnosti.txt', dodaj, 'utf-8', function(){
                                if(err) return err; 
                                brojac++;
                            });
                        }
                        else if(currentline[0].localeCompare(req.params.naziv.toString()) == 0){
                            brojac++;
                        }
                    }
                    if(brojac==0){
                        return res.json({message:"Greška - aktivnost nije obrisana!"});
                    }
                    else{
                        return res.json({message:"Uspješno obrisana aktivnost!"});
                    }
            });
           

        });
   


        app.delete('/v1/predmet/:naziv', (req, res) => { 
            let brojac = 0;
          fs.readFile('predmeti.txt', (err, data) => {
                if (err) return res.json({message:"Greška - predmet nije obrisan!"}); 
                let podaci = data.toString('utf8');
                const lines = podaci.split('\n');
                        fs.writeFile('predmeti.txt', '', function(){
                            if(err) return  res.json({message:"Greška - predmet nije obrisan!"}); 
                        });

                for(let i = 0; i<lines.length; i++) {
                    const currentline = lines[i];
                        if(currentline.localeCompare(req.params.naziv.toString()) != 0){
                            let dodaj = lines[i] + "\n";
                            fs.appendFile('predmeti.txt', dodaj, 'utf-8', function(){
                                if(err) return err; 
                            });
                        }
                        else if(currentline.localeCompare(req.params.naziv.toString()) == 0){
                            brojac++;
                        }
                    }
                    if(brojac==0){
                        return res.json({message:"Greška - predmet nije obrisan!"});
                    }
                    else {
                        return res.json({message:"Uspješno obrisan predmet!"});
                    }
            });
           
        });
    


        app.delete('/v1/all', (req, res) => {
            fs.readFile('predmeti.txt', (err, data) => {
                if (err) return err; 
                const headers = ["naziv"];
                        fs.writeFile('predmeti.txt', '', function(){
                            if(err) return res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"}); 
                        });



                        
                });
                    fs.readFile('aktivnosti.txt', (err, data) => {
                        if (err) return res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"}); 
                                fs.writeFile('aktivnosti.txt', '', function(){
                                    if(err) return res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"}); 
                                });

                        });
                        
        return res.json({message:"Uspješno obrisan sadržaj datoteka!"});      

            
        });


        //rute za get
     app.get('/v2/predmet', (req, res) => {
        db.Predmet.findAll().then(function (predmeti) {
            let lista = [];
            for (let i = 0; i < predmeti.length; i++) {
                noviObj = {id: predmeti[i].id, naziv: predmeti[i].naziv};
                lista.push(noviObj);
            }
            res.json(lista);
        });
     });

     app.get('/v2/grupa', (req, res) => {
        db.Grupa.findAll().then(function (grupe) {
            let lista = [];
            for (let i = 0; i < grupe.length; i++) {
                noviObj = {id: grupe[i].id, naziv: grupe[i].naziv, predmetId: grupe[i].PredmetId};
                lista.push(noviObj);
            }
            res.status(200);
            return res.json(lista);
        });
     });

     app.get('/v2/aktivnost', (req, res) => {
        db.Aktivnost.findAll().then(function (aktivnosti) {
            let lista = [];
            for (let i = 0; i < aktivnost.length; i++) {
                noviObj = {id:aktivnost[i].id, naziv: aktivnosti[i].naziv, pocetak:aktivnosti[i].pocetak, kraj: aktivnosti[i].kraj};
                lista.push(noviObj);
            }
            res.json(lista);
        });
     });

     app.get('/v2/dan', (req, res) => {
        db.Dan.findAll().then(function (dani) {
            let lista = [];
            for (let i = 0; i < dani.length; i++) {
                noviObj = {id: dani[i].id, naziv: dani[i].naziv};
                lista.push(noviObj);
            }
            res.json(lista);
        });
     });

     app.get('/v2/tip', (req, res) => {
        db.Tip.findAll().then(function (tipovi) {
            let lista = [];
            for (let i = 0; i < tipovi.length; i++) {
                noviObj = {id: tipovi[i].id, naziv: tipovi[i].naziv};
                lista.push(noviObj);
            }
            res.json(lista);
        });
     });
     
     app.get('/v2/student', (req, res) => {
        db.Student.findAll().then(function (studenti) {
            let lista = [];
            if(studenti!=null){
            for (let i = 0; i < studenti.length; i++) {
                noviObj = {id:studenti[i].id, ime: studenti[i].ime, index: studenti[i].index};
                lista.push(noviObj);
            }
        }
            console.log(lista);
            res.status(200);
            return res.json(lista);
        });
     });

     app.get('/v2/student_grupa', (req, res) => {
        db.StudentGrupa.findAll().then(function (studenti) {
            let lista = [];
            for (let i = 0; i < studenti.length; i++) {
                noviObj = {studentId:studenti[i].studentId, grupaId:studenti[i].grupaId};
                lista.push(noviObj);
            }

            console.log("lista 3" + lista);
            res.status(200);
            return res.json(lista);
        });
     });

     app.get('/v2/student/:ime/:index', (req,res) => {
        db.Student.findOne({
            where: {
                ime: req.params.ime,
                index: req.params.index
            }
        }).then(function(studenti){
            let lista = [];
            if(studenti!=null){
            for (let i = 0; i < studenti.length; i++) {
                noviObj = {id:studenti[i].id, ime: studenti[i].ime, index: studenti[i].index};
                lista.push(noviObj);
            }}
            console.log("lista 1" + lista);
        
            res.status(200);
            return res.json(lista);
        })
     });

     app.get('/v2/student/:index', (req,res) => {
        db.Student.findOne({
            where: {
                index: req.params.index
            }
        }).then(function(studenti){
            let lista = [];
            if(studenti!=null){
            for (let i = 0; i < studenti.length; i++) {
                noviObj = {id:studenti[i].id, ime: studenti[i].ime, index: studenti[i].index};
                lista.push(noviObj);
            }}
            console.log("lista " + lista);
            res.status(200);
            return res.json(lista);
        })
     });

     app.get('/v2/student_grupa/:studentId', (req,res) => {
        db.StudentGrupa.findAll({
            where: {
                studentId: req.params.studentId,
            }
        }).then(function(studenti){
            let lista = [];
            if(studenti!=null){
            for (let i = 0; i < studenti.length; i++) {
                noviObj = {studentId: studenti[i].studentId, grupaId: studenti[i].grupaId};
                lista.push(noviObj);
            }}
            console.log(lista);
            res.status(200);
            return res.json(lista);
        })
     });







    //rute za delete
    app.delete('/v2/predmet/:id', (req,res) => {
       db.Predmet.destroy({
           where: {
               id: req.params.id
           }
       }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
        if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisan predmet!"});
         }
      }, function(err){
        return res.json({message:"Greška - predmet nije obrisan!"});
      });
    });

    app.delete('/v2/aktivnost/:id', (req,res) => {
        db.Aktivnost.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
         if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisana aktivnost!"});
          }
       }, function(err){
        return res.json({message:"Greška - aktivnost nije obrisana!"});
       });
     });

     app.delete('/v2/grupa/:id', (req,res) => {
       
        db.Grupa.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
         if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisana grupa!"});
          }
       }, function(err){
        return res.json({message:"Greška - grupa nije obrisana!"});
       });
     });

     app.delete('/v2/tip/:id', (req,res) => {
    
        db.Tip.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
         if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisan tip!"});
          }
       }, function(err){
        return res.json({message:"Greška - tip nije obrisan!"});
       });
     });

     app.delete('/v2/dan/:id', (req,res) => {
    
        db.Dan.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
         if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisan dan!"});
          }
       }, function(err){
        return res.json({message:"Greška - dan nije obrisan!"});
       });
     });

     app.delete('/v2/student/:id', (req,res) => {
        db.Student.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(rowDeleted){ // rowDeleted will return number of rows deleted
         if(rowDeleted === 1){
            return res.json({message:"Uspješno obrisan student!"});
          }
       }, function(err){
        return res.json({message:"Greška - student nije obrisan!"});
       });
     });

     //rute za post
     app.post('/v2/predmet', (req, res) => {
       let logicka = 0;
       var nazivPredmeta = req.body.naziv;
        db.Predmet.findAll().then(function (predmeti) {
            for (let i = 0; i < predmeti.length; i++) {
               if(predmeti[i].naziv.toString().localeCompare(req.body.naziv)==0){
                   logicka+=1;
                   break;
               }  
            }
        });
    if(logicka==0){
        return db.Predmet.create({
            naziv: nazivPredmeta   
        }).then(function (model) {
            if (db.Predmet) {
                return res.json({message:"Uspješno dodan predmet!"});
            } else {
                res.status(400).send('Error in insert new record');
            }
        });
    }
    else {
        return res.json({message:"Naziv predmeta postoji!"});
    }
    });
    app.post('/v2/aktivnost', (req, res) => {
        return db.Aktivnost.create({
            naziv: req.body.naziv,
            pocetak: req.body.pocetak,
            kraj: req.body.kraj,
            PredmetId: req.body.predmetId,
            DanId: req.body.danId,
            TipId: req.body.tipId,
            GrupaId: req.body.grupaId
        }).then(function (users) {
            if (db.Aktivnost) {
                return res.json({message:"Uspješno dodana aktivnost!"});
            } else {
                response.status(400).send('Error in insert new record');
            }
        });
    });

    app.post('/v2/student', (req, res) => {
        let logicka = 0;
        var imeStudenta = req.body.ime;
        var brIndexa = req.body.index;
        console.log(imeStudenta);
         db.Student.findAll().then(function (studenti) {
             for (let i = 0; i < studenti.length; i++) {
                if(studenti[i].index==req.body.index && studenti[i].ime==req.body.ime){
                    logicka+=1;
                }  
             }
         });
     if(logicka==0){
         return db.Student.create({
             ime: imeStudenta,
             index: brIndexa   
         }).then(function (model) {
             if (db.Student) {
                 return res.json({message:"Uspješno dodan student!"});
             } else {
                 res.status(400).send('Error in insert new record');
             }
         });
     }
     else {
         return res.json({message:"Student sa istim brojem indeksa postoji u bazi!"});
     }
     });

     app.post('/v2/student_grupa', (req, res) => {
        let logicka = 0;
        var idStudenta = req.body.studentId;
        var idGrupe = req.body.grupaId;
        console.log(idStudenta);
         db.StudentGrupa.findAll().then(function (studentiIGrupe) {
             for (let i = 0; i < studentiIGrupe.length; i++) {
                if(studentiIGrupe[i].studentId==req.body.studentId && studentiIGrupe[i].grupaId==req.body.grupaId){
                    logicka+=1;
                }  
             }
         });
     if(logicka==0){
         return db.StudentGrupa.create({
            studentId: idStudenta,
            grupaId: idGrupe  
         }).then(function (model) {
             if (db.StudentGrupa) {
                 return res.json({message:"Uspješno dodan student i grupa!"});
             } else {
                 res.status(400).send('Error in insert new record');
             }
         });
     }
     else {
         return res.json({message:"Student i grupa postoje u bazi!"});
     }
     });

     app.post('/v2/dan', (req, res) => {
        let logicka = 0;
        var nazivDana = req.body.naziv;
         db.Dan.findAll().then(function (dani) {
             for (let i = 0; i < dani.length; i++) {
                if(dani[i].naziv.toString().localeCompare(req.body.naziv)==0){
                    logicka+=1;
                }  
             }
         });
     if(logicka==0){
         return db.Dan.create({
             naziv: nazivDana   
         }).then(function (model) {
             if (db.Dan) {
                 return res.json({message:"Uspješno dodan dan!"});
             } else {
                 res.status(400).send('Error in insert new record');
             }
         });
     }
     else {
         return res.json({message:"Naziv dana postoji!"});
     }
     });

     app.post('/v2/tip', (req, res) => {
        let logicka = 0;
        var nazivTipa = req.body.naziv;
         db.Tip.findAll().then(function (tipovi) {
             for (let i = 0; i < tipovi.length; i++) {
                if(ptipovi[i].naziv.toString().localeCompare(req.body.naziv)==0){
                    logicka+=1;
                }  
             }
         });
     if(logicka==0){
         return db.Tip.create({
             naziv: nazivTipa   
         }).then(function (model) {
             if (db.Tip) {
                 return res.json({message:"Uspješno dodan tip!"});
             } else {
                 res.status(400).send('Error in insert new record');
             }
         });
     }
     else {
         return res.json({message:"Naziv tipa postoji!"});
     }
     });

     app.post('/v2/grupa', (req, res) => {
        let logicka = 0;
        var nazivGrupe = req.body.naziv;
         db.Grupa.findAll().then(function (grupe) {
             for (let i = 0; i < grupe.length; i++) {
                if(grupe[i].naziv.toString().localeCompare(req.body.naziv)==0 && grupe[i].PredmetId.toString.localeCompare(req.body.PredmetId)==0){
                    logicka+=1;
                }  
             }
         });
     if(logicka==0){
         return db.Grupa.create({
             naziv: nazivGrupe,
            PredmetId: req.body.predmetId  
         }).then(function (model) {
             if (db.Grupa) {
                 return res.json({message:"Uspješno dodana grupa!"});
             } else {
                 res.status(400).send('Error in insert new record');
             }
         });
     }
     else {
         return res.json({message:"Naziv grupe postoji!"});
     }
     });
 //rute za put
     app.put('/v2/predmet/:id', (req, res) => {
      db.Predmet.update(
            // Values to update
            {
                naziv:  req.body.naziv
            },
            { // Clause
                where: 
                {
                    id: req.params.id
                }
            }
        ).then(count => {
            if(count>0){
                return res.json({message:"Uspješan update predmeta!"});
            }
            else {
                return res.json({message:"Neuspješan update predmeta!"});
            }
        });
     });


     app.put('/v2/student_grupa/:id', (req, res) => {
        db.StudentGrupa.update(
              // Values to update
              {
                  grupaId:  req.body.grupaId
              },
              { // Clause
                  where: 
                  {
                      studentId: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update grupe za studenta!"});
              }
              else {
                  return res.json({message:"Neuspješan update grupe za studenta!"});
              }
          });
       });

     app.put('/v2/aktivnost/:id', (req, res) => {
        db.Aktivnost.update(
              // Values to update
              {
                naziv: req.body.naziv,
                pocetak: req.body.pocetak,
                kraj: req.body.kraj,
                PredmetId: req.body.predmetId,
                DanId: req.body.danId,
                TipId: req.body.tipId,
                GrupaId: req.body.grupaId
              },
              { // Clause
                  where: 
                  {
                      id: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update aktivnosti!"});
              }
              else {
                  return res.json({message:"Neuspješan update aktivnosti!"});
              }
          });
       });

       app.put('/v2/grupa/:id', (req, res) => {
        db.Grupa.update(
              // Values to update
              {
                  naziv:  req.body.naziv,
                  PredmetId: req.body.PredmetId
              },
              { // Clause
                  where: 
                  {
                      id: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update grupe!"});
              }
              else {
                  return res.json({message:"Neuspješan update grupe!"});
              }
          });
       });
       app.put('/v2/dan/:id', (req, res) => {
        db.Dan.update(
              // Values to update
              {
                  naziv:  req.body.naziv
              },
              { // Clause
                  where: 
                  {
                      id: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update dana!"});
              }
              else {
                  return res.json({message:"Neuspješan update dana!"});
              }
          });
       });
       app.put('/v2/tip/:id', (req, res) => {
        db.Tip.update(
              // Values to update
              {
                  naziv:  req.body.naziv
              },
              { // Clause
                  where: 
                  {
                      id: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update tipa!"});
              }
              else {
                  return res.json({message:"Neuspješan update tipa!"});
              }
          });
       });

       app.put('/v2/student/:id', (req, res) => {
        db.Student.update(
              // Values to update
              {
                  ime:  req.body.ime,
                  index: req.body.index
              },
              { // Clause
                  where: 
                  {
                      id: req.params.id
                  }
              }
          ).then(count => {
              if(count>0){
                  return res.json({message:"Uspješan update studenta!"});
              }
              else {
                  return res.json({message:"Neuspješan update studenta!"});
              }
          });
       });
    app.listen(3000);

