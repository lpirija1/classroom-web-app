const Sequelize = require('sequelize');
const sequelize = new Sequelize('wt2018238', 'root', 'root', {
   host: 'localhost',
   dialect: 'mysql'
});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;


db.Predmet = sequelize.import(__dirname+'/predmet.js');
db.Grupa = sequelize.import(__dirname+'/grupa.js');
db.Aktivnost = sequelize.import(__dirname+'/aktivnost.js');
db.Dan = sequelize.import(__dirname+'/dan.js');
db.Tip = sequelize.import(__dirname+'/tip.js');
db.Student = sequelize.import(__dirname+'/student.js');


//Predmet 1-N Grupa
 db.Predmet.hasMany(db.Grupa, {as: 'grupe'});
 db.Grupa.belongsTo(db.Predmet);
 //Aktivnost N-1 Predmet
db.Predmet.hasMany(db.Aktivnost, {as: 'aktivnosti'});
db.Aktivnost.belongsTo(db.Predmet);

//Aktivnost N-1 Dan
db.Dan.hasMany(db.Aktivnost, {as: 'aktivnosti'});
db.Aktivnost.belongsTo(db.Dan);

//Aktivnost N-1 Tip
db.Tip.hasMany(db.Aktivnost, {as: 'aktivnosti'});
db.Aktivnost.belongsTo(db.Tip);

//Student N-M Grupa
db.StudentGrupa = sequelize.define('student_grupa');
db.Student.belongsToMany(db.Grupa, {
    through: 'student_grupa',
    as: 'Grupa',
    foreignKey: 'studentId'
});

db.Grupa.belongsToMany(db.Student, {
    through: 'student_grupa',
    as: 'Student',
    foreignKey: 'grupaId'
});

//Aktivnost N-0 Grupa
db.Aktivnost.belongsTo(db.Grupa);
db.Grupa.hasMany(db.Aktivnost);

module.exports=db;



