function dajGrupeIzBaze() {
	var ajax = new XMLHttpRequest();
    let grupe;
    ajax.onreadystatechange = function() {
        if (ajax.readyState==4 && ajax.status == 200) 
        {
          grupe = JSON.parse(ajax.responseText); 
          var select = document.getElementById("grupe");
          for(let i = 0; i < grupe.length; i++) {
          var option = document.createElement('option');
		  option.text = option.value =grupe[i].naziv;
		 
          select.add(option, 0);
            }
        }
    }
    ajax.open("GET","http://localhost:3000/v2/grupa",true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}
dajGrupeIzBaze();

    function ucitajSaForme(studenti,grupa) {
      var studentiSaForme = studenti.split('\n');
      let poruke = "";
      let jedanStudent;
      let studentiUBazi;
      let idZaNovog;
      let idGrupe;
      let vecUTabeli;
      let url;
      let idStudenta;
      var ajaxZaGrupu = new XMLHttpRequest();
      ajaxZaGrupu.onreadystatechange = function() {
        if (ajaxZaGrupu.readyState==4 && ajaxZaGrupu.status == 200) {
          let grupe = JSON.parse(ajaxZaGrupu.responseText);
         for(let i = 0; i<grupe.length; i++){
             if(grupe[i].naziv==grupa){
               idGrupe = grupe[i].id;
             }
         }
          var ajax = new XMLHttpRequest();
          ajax.onreadystatechange = function() {
          if (ajax.readyState==4 && ajax.status == 200) {
          studentiUBazi = JSON.parse(ajax.responseText);
          if(studentiUBazi.length==0) idZaNovog=0;
         else idZaNovog = studentiUBazi[studentiUBazi.length-1].id;
          
      for(let i = 0; i<studentiSaForme.length; i++) {
       jedanStudent = studentiSaForme[i].split(',');
       let istiStudent = 0;
       let istiIndex = 0;
       let zapamtiIstog;
      for(let s=0; s<studentiUBazi.length; s++){
          if(studentiUBazi[s].ime==jedanStudent[0] && studentiUBazi[s].index==jedanStudent[1]){
             istiStudent=1;
             idStudenta = studentiUBazi[s].id;
          }
          else if(studentiUBazi[s].index==jedanStudent[1]){
            istiIndex = 1;
            zapamtiIstog=s;
          }
      }
       if(istiStudent==0 && istiIndex==0){
       idZaNovog++;
              var ajax2 = new XMLHttpRequest();
              ajax2.onreadystatechange = function() {
               if (ajax2.readyState == 4 && ajax2.status == 200) {
                      var ajax3 = new XMLHttpRequest();
                        ajax3.onreadystatechange = function() {
                          if(ajax3.readyState==4 && ajax3.status==200)
                           document.getElementById("studenti").value=poruke;
                        }
                        ajax3.open("POST", "http://localhost:3000/v2/student_grupa", true);
                        ajax3.setRequestHeader("Content-Type", "application/json");
                        ajax3.send(JSON.stringify({studentId:idZaNovog, grupaId:idGrupe})); 
            }
            }
            ajax2.open("POST", "http://localhost:3000/v2/student", true);
            ajax2.setRequestHeader("Content-Type", "application/json");
            ajax2.send(JSON.stringify({ime:jedanStudent[0], index:jedanStudent[1]}));
            }
  else if(istiStudent==1){
    poruke+="Student sa imenom " + jedanStudent[0] + " i indeksom " + jedanStudent[1] + " već postoji u bazi" +"\n";
    url = "http://localhost:3000/v2/student_grupa/"+ idStudenta.toString();
    var ajaxZaGrupu = new XMLHttpRequest();
    ajaxZaGrupu.onreadystatechange = function() {
      if(ajaxZaGrupu.readyState==4 && ajaxZaGrupu.status==200){
          vecUTabeli = JSON.parse(ajaxZaGrupu.responseText);
          if(vecUTabeli.length==0){
          var ajax3 = new XMLHttpRequest();
            ajax3.onreadystatechange = function() {
              if(ajax3.readyState==4 && ajax3.status==200)
                    document.getElementById("studenti").value=poruke;
                }
              ajax3.open("POST", "http://localhost:3000/v2/student_grupa", true);
              ajax3.setRequestHeader("Content-Type", "application/json");
              ajax3.send(JSON.stringify({studentId:idStudenta, grupaId:idGrupe})); 
          }
          else{
            var ajax3 = new XMLHttpRequest();
            ajax3.onreadystatechange = function() {
              if(ajax3.readyState==4 && ajax3.status==200)
                    document.getElementById("studenti").value=poruke;
                }
              ajax3.open("PUT", url, true);
              ajax3.setRequestHeader("Content-Type", "application/json");
              ajax3.send(JSON.stringify({grupaId:idGrupe})); 
          }
      }
    }
    ajaxZaGrupu.open("GET",url, true);
    ajaxZaGrupu.setRequestHeader("Content-Type", "application/json");
    ajaxZaGrupu.send(JSON.stringify({grupaId:idGrupe}));
  }
  else if(istiIndex==1){
    poruke+="Student " + jedanStudent[0] + " nije kreiran jer postoji student " + studentiUBazi[zapamtiIstog].ime + " sa indexom " + studentiUBazi[zapamtiIstog].index + "\n";
     if(i==studentiSaForme.length-1){
       document.getElementById("studenti").value=poruke;
     }
  }

  }//for
}
}
ajax.open("GET","http://localhost:3000/v2/student",true);
ajax.setRequestHeader("Content-Type", "application/json");
ajax.send();
        }
      }
      ajaxZaGrupu.open("GET","http://localhost:3000/v2/grupa",true);
ajaxZaGrupu.setRequestHeader("Content-Type", "application/json");
ajaxZaGrupu.send();
}//fun