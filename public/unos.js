function dajDaneIzBaze() {
    var ajax = new XMLHttpRequest();
    let dani;
    ajax.onreadystatechange = function() {
        if (ajax.readyState==4 && ajax.status == 200) 
        {
		  dani = JSON.parse(ajax.responseText);
		  var select = document.getElementById("days");
          for(let i = 0; i < dani.length; i++) {
          var option = document.createElement('option');
		  option.text =  option.value = dani[i].naziv;
		
          select.add(option, 0);
            }
        }
    }
    ajax.open("GET","http://localhost:3000/v2/dan",true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}
dajDaneIzBaze();

function dajTipoveIzBaze() {
	var ajax = new XMLHttpRequest();
    let tipovi;
    ajax.onreadystatechange = function() {
        if (ajax.readyState==4 && ajax.status == 200) 
        {
          tipovi = JSON.parse(ajax.responseText); 
          var select = document.getElementById("type");
          for(let i = 0; i < tipovi.length; i++) {
          var option = document.createElement('option');
		  option.text = option.value = tipovi[i].naziv;
		  
          select.add(option, 0);
            }
        }
    }
    ajax.open("GET","http://localhost:3000/v2/tip",true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}
dajTipoveIzBaze();

function dajGrupeIzBaze() {
	var ajax = new XMLHttpRequest();
    let grupe;
    ajax.onreadystatechange = function() {
        if (ajax.readyState==4 && ajax.status == 200) 
        {
          grupe = JSON.parse(ajax.responseText); 
          var select = document.getElementById("grupe");
          for(let i = 0; i < grupe.length; i++) {
          var option = document.createElement('option');
		  option.text = option.value =grupe[i].naziv;
		 
          select.add(option, 0);
            }
        }
    }
    ajax.open("GET","http://localhost:3000/v2/grupa",true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}
dajGrupeIzBaze();


function dajPredmeteIzBaze() {
	var ajax = new XMLHttpRequest();
    let predmeti;
    ajax.onreadystatechange = function() {
        if (ajax.readyState==4 && ajax.status == 200) 
        {
          predmeti = JSON.parse(ajax.responseText); 
          var select = document.getElementById("predmeti");
          for(let i = 0; i < predmeti.length; i++) {
          var option = document.createElement('option');
		  option.text = option.value = predmeti[i].naziv;
		 
          select.add(option, 0);
            }
        }
    }
    ajax.open("GET","http://localhost:3000/v2/predmet",true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send();
}
dajPredmeteIzBaze();



function ucitajSaForme(predmet, tip, pocetak, kraj, dan, grupa) {
  
	var predmeti; 
	var dani;
	var tipovi;
	var predmetID;
	var tipID;
	var danID;
	var grupaID;

	var ajax = new XMLHttpRequest;
	ajax.onreadystatechange = function() 
	{
		if (ajax.readyState == 4 && ajax.status == 200) 
		{
		  predmeti = JSON.parse(ajax.responseText);
		  for(let i = 0; i<predmeti.length; i++){
			  if(predmeti[i].naziv==predmet) predmetID = predmeti[i].id;
		  }

			var ajax1 = new XMLHttpRequest;
			ajax1.onreadystatechange = function(){
				if (ajax.readyState == 4 && ajax.status == 200) 
				{
					 grupe = JSON.parse(ajax1.responseText);
					 for(let i = 0; i<grupe.length; i++){
						if(grupe[i].naziv==grupa) grupaID = grupe[i].id;
					}
					
					var ajax2 = new XMLHttpRequest;
			ajax2.onreadystatechange = function(){
				if (ajax2.readyState == 4 && ajax2.status == 200) {
					dani = JSON.parse(ajax2.responseText);
					for(let i = 0; i<dani.length; i++){
						if(dani[i].naziv==dan) danID = dani[i].id;
					}

					var ajax3 = new XMLHttpRequest;
					ajax3.onreadystatechange = function(){
						if (ajax3.readyState == 4 && ajax3.status == 200) {
							tipovi = JSON.parse(ajax3.responseText);
							for(let i = 0; i<tipovi.length; i++){
								if(tipovi[i].naziv==tip) tipID = tipovi[i].id;
							}

							
							var pocetakPomocna = pocetak.toString().split(':');
							var krajPomocna = kraj.toString().split(':');
							var pomocni1 = parseInt(pocetakPomocna[0]) + parseInt(pocetakPomocna[1]);
							var pomocni2 = parseInt(krajPomocna[0]) + parseInt(krajPomocna[1]);

							var ajax5= new XMLHttpRequest;
							ajax5.onreadystatechange = function() {}
							ajax5.open("POST", "http://localhost:3000/v2/aktivnost", true);
							ajax5.setRequestHeader("Content-Type", "application/json");
							ajax5.send(JSON.stringify({naziv:predmet,pocetak:pomocni1,kraj:pomocni2,predmetId: predmetID, danId: danID, tipId: tipID, grupaId: grupaID}));
							
						}
					}
					ajax3.open("GET","http://localhost:3000/v2/tip",true);
	                ajax3.setRequestHeader("Content-Type", "application/json");
	                ajax3.send();


				}
			}
			ajax2.open("GET","http://localhost:3000/v2/dan",true);
	        ajax2.setRequestHeader("Content-Type", "application/json");
	        ajax2.send();

					

			}
		    }
			ajax1.open("GET","http://localhost:3000/v2/grupa",true);
	        ajax1.setRequestHeader("Content-Type", "application/json");
	        ajax1.send();

		}
	}
	ajax.open("GET","http://localhost:3000/v2/predmet",true);
	ajax.setRequestHeader("Content-Type", "application/json");
	ajax.send();
}
