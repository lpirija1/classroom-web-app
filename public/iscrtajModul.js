var raspored = (function(){
var iscrtajRaspored = function(div,dani,satPocetak,satKraj)
{
    

    if(div==null) {
        return "Greška";
    }

    else if(satPocetak>=satKraj || !Number.isInteger(satPocetak) || !Number.isInteger(satKraj)) {
        let poruka=document.createElement('p');
        poruka.innerHTML = 'Greška';
        div.appendChild(poruka);
    }
    else {

var tabela = document.createElement('table');

var dan = 0;

for (i = 0; i < dani.length + 1 ;  i++) {
    var red = document.createElement('tr');
    if(i != 0){
    for (j = 0; j <(satKraj - satPocetak)*2+1; j++) {
        var celija = document.createElement('td');
        var paragraf = document.createElement('p');
        paragraf.innerHTML = "" ;
        if(j == 0 && i>0) {
            paragraf.innerHTML = dani[dan++];
        }
        if(j == 0) {
            celija.setAttribute("class", "prva");
        }
        else if(j%2==0 && j!= (satKraj - satPocetak)*2 ) {
            celija.setAttribute("class", "parna");
        }
        else if(i != 0 && j == (satKraj - satPocetak)*2){
            celija.setAttribute("class", "zadnjaKol");
        }
        else {
            celija.setAttribute("class", "neparna");
        }

        celija.setAttribute('colspan', 0);
        celija.setAttribute('value', "prazno");
        celija.appendChild(paragraf);
        red.appendChild(celija);
    }
}
else {
        var m;
        for(m = satPocetak; m<satKraj; m++){
            var celija = document.createElement('td');
            var paragraf = document.createElement('p');
            celija.setAttribute('colspan', "2");
            celija.setAttribute("class", "prvi");
    
            if(m<=12 && m%2 == 0 || m>=15 && m%2==1) {
                if(m<10){
                paragraf.innerHTML = "0" + m + ":00";
                }
                else {
                paragraf.innerHTML = m + ":00";
                }
            }
            celija.appendChild(paragraf);
            red.appendChild(celija);
        }
        
}
tabela.appendChild(red);
}

div.appendChild(tabela);
    }
}


var dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj,dan){
      
   if(raspored == null) {
    return ("Greška - raspored nije kreiran");
     }
 
    else if(Math.abs(vrijemePocetak-Number.parseInt(vrijemePocetak))!=0 && Math.abs(vrijemePocetak-Number.parseInt(vrijemePocetak))!=0.5 || Math.abs(vrijemeKraj-Number.parseInt(vrijemeKraj))!=0 && Math.abs(vrijemeKraj-Number.parseInt(vrijemeKraj))!=0.5 ){
     return ("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
     
     }
     else{
 
 var rasp = document.getElementById(raspored.id).children;
 var stringPocetak = rasp[0].rows[0].cells[0].textContent;
 if(stringPocetak == ""){
     stringPocetak = rasp[0].rows[0].cells[1].textContent;
     var pocetakRasporeda = parseInt(stringPocetak);
     pocetakRasporeda -= 1;
 }
 else 
     var pocetakRasporeda = parseInt(stringPocetak);
 
 
     var k = 0;
     while(rasp[0].rows[0].cells[k]!=null){
         k++;
     }
 
 var krajRasporeda = pocetakRasporeda+k;
 
 if(vrijemePocetak>vrijemeKraj || vrijemePocetak<0 || vrijemePocetak>24 || vrijemeKraj<0 || vrijemeKraj>24 || vrijemePocetak<pocetakRasporeda || vrijemeKraj<pocetakRasporeda 
     || vrijemePocetak>krajRasporeda || vrijemeKraj>krajRasporeda){
     return ("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    
 }
 
 
 
 var red = -1;
 for(i = 1; i<rasp[0].children.length; i++){
     if((rasp[0].rows[i].cells[0].textContent).localeCompare(dan)==0) red = i;
 }
 
 if(red==-1) {
     return ("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
     
 }
 
 
 var vrijeme = pocetakRasporeda;
 var kolona;
 
 //u koju kolonu treba upisati aktivnost
 var j = 1;
 var colspan = 0;
 while(vrijeme<vrijemePocetak) {
     if(rasp[0].rows[red].cells[j].getAttribute("colspan")!=null){
         if(parseInt(rasp[0].rows[red].cells[j].getAttribute("colspan"))!=0)
      colspan+=(parseInt(rasp[0].rows[red].cells[j].getAttribute("colspan"))-1);
     }
 j++;
 vrijeme+=0.5;
 }
 kolona = j-colspan;
 
 
  //da li je zauzeta ta kolona
 var brisanje = (vrijemeKraj - vrijemePocetak)*2;
 var imaPreklapanja = false;
 if(rasp[0].rows[red].cells[kolona].getAttribute("value")!=null && rasp[0].rows[red].cells[kolona].getAttribute("value")=="popunjeno") { 
     imaPreklapanja = true; 
     return("Greška - već postoji termin u rasporedu u zadanom vremenu");
 }
 else{
 
 //da li ima preklapanja sa nekom postojećom aktivnosti
 var m;
 for(m = kolona+1; m<kolona+brisanje; m++) {   
     if(rasp[0].rows[red].cells[m].getAttribute("value")!=null && rasp[0].rows[red].cells[m].getAttribute("value")=="popunjeno"){
         imaPreklapanja = true;
         return ("Greška - već postoji termin u rasporedu u zadanom vremenu");
         
     }
   
 }
 }
 if(!imaPreklapanja){
     
     rasp[0].rows[red].cells[kolona].setAttribute('colspan', brisanje);
     var n = 0;
     var brisiKolonu = kolona+1;
     while(n<=brisanje-2) {
         rasp[0].rows[red].deleteCell(brisiKolonu);
         n++;      
     }
 
     if(rasp[0].rows[red].cells[kolona+1]==null && Number.isInteger(vrijemePocetak)) {
         rasp[0].rows[red].cells[kolona].setAttribute("class", "zadnjaCelija1");
     }
     else if(rasp[0].rows[red].cells[kolona+1]==null && !Number.isInteger(vrijemePocetak)){
         rasp[0].rows[red].cells[kolona].setAttribute("class", "zadnjaCelija2");
     }
 
 
     var hTag = document.createElement('h');
     var paragraf = document.createElement('p');
    
    hTag.innerHTML = naziv;
    paragraf.innerHTML = tip;
     rasp[0].rows[red].cells[kolona].appendChild(hTag);
     rasp[0].rows[red].cells[kolona].appendChild(paragraf);
     rasp[0].rows[red].cells[kolona].setAttribute('value', "popunjeno");
     rasp[0].rows[red].cells[kolona].style.backgroundColor = "#dee6ef";
 }
     }
}
return {

    iscrtajRaspored: iscrtajRaspored,
    dodajAktivnost: dodajAktivnost
}
}());


raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak", "Utorak", "Srijeda", "Četvrtak","Petak"],8,21);
