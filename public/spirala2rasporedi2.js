
    let assert = chai.assert;
describe('raspored', function() {
 describe('iscrtajRaspored ()', function() {
    it('provjera ispisa vremena početka rasporeda kada je početak neparan sat', function() {
        raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"], 5,20);
        let tabela = document.getElementsByTagName("table");
        let ispis = tabela[tabela.length-1].children[0].children[0].children[0].textContent;
        assert.equal(ispis, "");
      });

    it('provjera ispisa vremena početka rasporeda kad je početak paran broj manji od 12', function(){
        raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"], 10,21);
        let tabela = document.getElementsByTagName("table");
        let ispis = tabela[tabela.length-1].children[0].children[0].children[0].textContent;
        assert.equal(ispis, "10:00");  
    });

    it('provjera ispisa vremena početka rasporeda kad je početak paran broj veći od 12', function(){
        raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"], 16,21);
        let tabela = document.getElementsByTagName("table");
        let ispis = tabela[tabela.length-1].children[0].children[0].children[0].textContent;
        assert.equal(ispis, "");  
    });

    it('provjera broja redova u rasporedu', function() {
    raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"], 5,20);
    let tabele = document.getElementsByTagName("table");
    let tabela = tabele[tabele.length-1];
    let redovi = tabela.getElementsByTagName("tr");
    assert.equal(redovi.length, 6,"Broj redova treba biti 6");
    });

    it('provjera da li je zadnji sat prikazan na rasporedu', function() {
    raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak"], 9,19);
    let tabele = document.getElementsByTagName("table");
    let td = tabele[tabele.length-1].children[0].children[0].children.length;
    let ispis = tabele[tabele.length-1].children[0].children[0].children[td-1].textContent;
    assert.equal(ispis, "");
    });

    it('provjera broja kolona u rasporedu', function() {
    raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak"], 9,19);
    let tabele = document.getElementsByTagName("table");
    let brojKolona = tabele[tabele.length-1].children[1].children.length;
    assert.equal(brojKolona, 21, "Broj kolona treba biti 21");
    });

    it('provjera poruke kada raspored nije kreiran', function() {
         var poruka  = raspored.iscrtajRaspored(null,["Ponedjeljak","Utorak","Srijeda","Četvrtak"], 9,19);
        assert.equal(poruka, "Greška");
    });

    it('provjera poruke kada je početak rasporeda decimalni broj', function() {
       raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak", "Petak"], 6.15,17);
        assert.equal(document.getElementsByTagName("p")[document.getElementsByTagName("p").length-1].innerHTML, "Greška");
        });
    it('provjera poruke kada je kraj rasporeda decimalni broj', function() {
            raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak", "Petak"], 13,17.8);
             assert.equal(document.getElementsByTagName("p")[document.getElementsByTagName("p").length-1].innerHTML, "Greška");
    });
    it('provjera poruke kada je sat početka rasporeda veći od sata kraja rasporeda', function() {
        raspored.iscrtajRaspored(document.getElementById("tabela"),["Ponedjeljak","Utorak","Srijeda","Četvrtak", "Petak"], 13, 11);
         assert.equal(document.getElementsByTagName("p")[document.getElementsByTagName("p").length-1].innerHTML, "Greška");
    });

});

describe('dodajAktivnost ()', function() {
    it('provjera poruke kada raspored nije kreiran', function() {
    let poruka  = raspored.dodajAktivnost(null,"WT","predavanje",9,12,"Ponedjeljak");
    assert.equal(poruka, "Greška - raspored nije kreiran");
    alert(poruka);
   });
   it('provjera poruke kada je vrijeme početka aktivnosti veće od vremena kraja aktivnosti', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",12,9,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada vrijeme početka aktivnosti nije u ispravnom formatu', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",12.7,15,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada vrijeme kraja aktivnosti nije u ispravnom formatu', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",12,15.4,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada je vrijeme početka aktivnosti prije vremena početka rasporeda', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",5,9,"Utorak");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada je vrijeme početka aktivnosti manje od 0', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",-2,15,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada je vrijeme kraja aktivnosti manje od 0', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",2,-15,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada je vrijeme početka aktivnosti veće od 24', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",26,15,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera poruke kada je vrijeme kraja aktivnosti veće od 24', function() {
    let poruka  = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",2,30,"Srijeda");
    assert.equal(poruka, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");
    alert(poruka);
   });

   it('provjera da li je aktivnost dodana u ispravan red i ispravnu kolonu u rasporedu, te da li je ispravna širina koju zauzima', function() {
    raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",9,12,"Ponedjeljak");
    let rasp = document.getElementsByTagName("table");
    assert.equal(rasp[0].children[1].children[3].getAttribute("colspan"), 6);
   });
   it('provjera da li je aktivnost dodana u ispravan red i ispravnu kolonu u rasporedu, te da li je ispravna širina koju zauzima, ako je pocetak decimalni broj', function() {
    raspored.dodajAktivnost(document.getElementById("tabela"),"WT","predavanje",11.5,13,"Srijeda");
    let rasp = document.getElementsByTagName("table");
    assert.equal(rasp[0].children[3].children[8].getAttribute("colspan"), 3);
   });
   it('provjera da li je aktivnost dodana u ispravan red i ispravnu kolonu u rasporedu, te da li je ispravna širina koju zauzima, ako su početak i kraj decimalni brojevi', function() {
    raspored.dodajAktivnost(document.getElementById("tabela"),"RMA","predavanje",11.5,13.5,"Petak");
    let rasp = document.getElementsByTagName("table");
    assert.equal(rasp[0].children[5].children[8].getAttribute("colspan"), 4);
   });
   it('provjera preklapanja kada već postoji aktivnost koja počinje u isto vrijeme', function() {
    let poruka = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","vježbe",11.5,13.5,"Petak");
    assert.equal(poruka, "Greška - već postoji termin u rasporedu u zadanom vremenu");
    alert(poruka);
   });

   it('provjera preklapanja kada postoji aktivnost koja još traje', function() {
    let poruka = raspored.dodajAktivnost(document.getElementById("tabela"),"WT","vježbe",12,13.5,"Petak");
    assert.equal(poruka, "Greška - već postoji termin u rasporedu u zadanom vremenu");
    alert(poruka);
   });
});


});